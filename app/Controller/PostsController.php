<?php
/**
 * Created by PhpStorm.
 * User: YNS
 * Date: 04/05/2017
 * Time: 4:56 PM
 * Author: Dave Perez
 */

App::uses('AppController', 'Controller');

class PostsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('index, view'));
    }

    public function index() {
        $this->set('posts', $this->Post->find('all'));
    }

    public function add() {
        $this->set("id", $this->Auth->user('id'));

        if ($this->request->is('post')) {
            $this->Post->create();
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Post created successfully.'));
                $this->Auth->login();
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Post not created.'));
        }
    }
}