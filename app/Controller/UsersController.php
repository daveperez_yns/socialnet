<?php
/**
 * Created by PhpStorm.
 * User: YNS
 * Date: 04/05/2017
 * Time: 11:22 AM
 * Author: Dave Perez
 */
App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public $components = array('Paginator');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('logout');
    }

    public function index() {
        $this->paginate = array(
            'fields' => array(
                'User.id', 'User.first_name', 'User.last_name', 'User.username', 'User.email'
            ),
            'maxLimit' => 5,
            'order' => array('User.id', 'desc')
        );

        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('User not found.'));
        }
        $this->set('owner', $this->Auth->User('id') == $id);

        $this->set('user', $this->User->findById($id));
    }

    public function register() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Registration successful.'));
                $this->Auth->login();
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unsuccessful registration.'));
        }
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('User does not exist.'));
        }

        $user = $this->User->findById($id);

        if (!$user) {
            throw new NotFoundException(__('User does not exist.'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->User->id = $id;
            if ($this->User->save($this->request->data)) {
                $this->Flash->success(__('Profile updated successfully.'));
                return $this->redirect(array('action' => 'view', $id));
            }
            $this->Flash->error(__('Changes not saved.'));
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        }
    }

    public function delete($id = null) {
        if (!$id) {
            throw new NotFoundException(__('User does not exist.'));
        }

        $user = $this->User->findById($id);

        if (!$user) {
            throw new NotFoundException(__('User does not exist.'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->User->id = $id;
            $this->User->read(null, 1);
            $this->User->set(array(
                'deleted' => 1,
                'deleted_date' => date("Y-m-d H:i:s")
            ));
            if ($this->User->save()) {
                $this->Flash->success(__('Profile has been deactivated.'));
                return $this->redirect(array('action' => 'logout'));
            }
            $this->Flash->error(__('Incorrect Password .'));
        }

        if (!$this->request->data) {
            $this->request->data = $user;
        }
    }

    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }
}