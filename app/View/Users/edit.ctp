<h1>Edit Profile</h1>
<?php
echo $this->Form->create('User');
echo $this->Form->input('first_name');
echo $this->Form->input('middle_name');
echo $this->Form->input('last_name');
echo $this->Form->input('username');
echo $this->Form->input('email');
echo $this->Form->input('id', array('type' => 'hidden'));
echo $this->Form->end('Save Changes');
?>

<script>
    $(document).ready(function(){
        $('.edit-profile').addClass('active');
    });
</script>