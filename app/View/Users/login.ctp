<?php
echo $this->Flash->render('auth');
echo $this->Form->create('User');
echo $this->Form->input('username');
echo $this->Form->input('password');
echo $this->Form->end(__('Login'));

?>

<script>
    $(document).ready(function(){
        $('.submit').append('<a class="default-button register" href="/users/register">Register</a>');
    });
</script>