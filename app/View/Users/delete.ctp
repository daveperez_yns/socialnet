<?php
echo $this->Flash->render('auth');
echo $this->Form->create('User');
echo $this->Form->input('current_password', array('type' => 'password'));
echo $this->Form->end(__('Deactivate'));

?>

<script>
    $(document).ready(function(){
        $('.deactivate').addClass('active');
    });
</script>