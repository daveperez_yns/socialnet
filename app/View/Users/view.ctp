<div id="profile">
    <div id="name_space">
        <?php echo $user['User']['first_name'] ?> <?php echo $user['User']['last_name'] ?>
            (<?php echo $user['User']['username'] ?>)
        <button class="frnd-action">Add Friend</button>
    </div>
    <?php if ($owner) { ?>
    <div id="status_box">
        <?php
        echo $this->Form->create('Post');
        echo $this->Form->input('content', ['type' => 'textarea', 'label' => false,
        'placeholder'=> 'Share your thoughts...', 'escape' => false, 'rows' => '10']);
        echo $this->Form->end('Post');
        ?>

    </div>
    <?php } ?>
    <div id="post_container">
        <h1>Posts</h1>
    </div>

</div>
<script>
    $(document).ready(function(){
        $('.view-profile').addClass('active');
    });
</script>