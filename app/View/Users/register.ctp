<?php
echo $this->Form->create('User');
echo $this->Form->input('first_name');
echo $this->Form->input('middle_name');
echo $this->Form->input('last_name');
echo $this->Form->input('username');
echo $this->Form->input('email');
echo $this->Form->input('password');
echo $this->Form->input('confirm_password', array('type' => 'password'));
echo $this->Form->end('Register');
?>
<script>
    $(document).ready(function(){
        $('.submit').append('<input class="default-button" type="reset" value="Reset">');
    });
</script>
