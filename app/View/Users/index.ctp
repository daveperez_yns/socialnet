<h1>List of Users</h1>
<table>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Username</th>
        <th></th>
    </tr>

    <!-- Here is where we loop through our $users array, printing out user info -->

    <?php foreach ($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['first_name'] . ' ' . $user['User']['last_name']; ?></td>
        <td><?php echo $user['User']['email']; ?></td>
        <td>
            <?php echo $this->Html->link($user['User']['username'],
            array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>
        </td>
        <td><button class="frnd-action">Add Friend</button></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($user); ?>
</table>
<div id="user_pagination">
    <?php echo $this->Paginator->prev('« Previous', null, null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->next('Next »', null, null, array('class' => 'disabled')); ?>
</div>
<script>
    $(document).ready(function(){
        $('.find-more').addClass('active');
    });
</script>