<?php
if ($this->Session->read('Auth.User')) {
?>
<div class="sidebar">
    <div class="menu-list">
        <ul id="menu-content" class="menu-content collapse out">
            <li data-toggle="collapse" class="collapsed">

            </li>
            <li data-toggle="collapse" class="collapsed">
                Profile
            </li>
            <ul class="sub-menu collapse">
                <li class="view-profile"><?php
                    echo $this->Html->link('View Profile',
                    array('controller' => 'users', 'action' => 'view', $this->Session->read('Auth.User.id')));
                    ?>
                </li>
                <li class="edit-profile"><?php
                    echo $this->Html->link('Edit Profile',
                    array('controller' => 'users', 'action' => 'edit', $this->Session->read('Auth.User.id')));
                    ?>
                </li>
                <li class="change-password"><a href="#">Change Password</a></li>
                <li class="deactivate"><?php
                    echo $this->Html->link('Deactivate',
                    array('controller' => 'users', 'action' => 'delete', $this->Session->read('Auth.User.id')));
                    ?>
                </li>
                <li><?php
                    echo $this->Html->link('Logout',
                    array('controller' => 'users', 'action' => 'logout'));
                    ?>
                </li>
            </ul>

            <li data-toggle="collapse" data-target="#service" class="collapsed">
               Messages
            </li>
            <ul class="sub-menu collapse" id="service">
                <li>View All</li>
                <li>Create New</li>
            </ul>

            <li data-toggle="collapse" data-target="#new" class="collapsed">
                Friendships
            </li>
            <ul class="sub-menu collapse" id="new">
                <li>View All</li>
                <li class="find-more"><?php
                    echo $this->Html->link('Find More',
                    array('controller' => 'users', 'action' => 'index'));
                    ?>
                </li>
            </ul>
        </ul>
    </div>
</div>
<?php } ?>