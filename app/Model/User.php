<?php
/**
 * Created by PhpStorm.
 * User: YNS
 * Date: 04/05/2017
 * Time: 11:26 AM
 * Author: Dave Perez
 */

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    public $validate = array(
        'first_name' => array(
            'rule' => 'notBlank',
            'message' => 'Please provide your first name.'
        ),
        'last_name' => array(
            'rule' => 'notBlank',
            'message' => 'Please provide your last name.'
        ),
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please provide a username.',
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'This username has already been taken.'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please provide an email.',
            ),
            'validEmail' => array(
                'rule' => array('email', true),
                'message' => 'Please supply a valid email address.'
            )

        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Password is required.',
            ),
            'between' => array(
                'rule' => array('lengthBetween', 6, 15),
                'message' => 'Please input minimum of 6 characters up to 15 characters.'
            )
        ),
        'confirm_password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Please confirm password.',
            ),
            'confirm_matching' => array(
                'rule' => array('validatePassword', 'password'),
                'message' => 'Password does not match.'
            ),
            'between' => array(
                'rule' => array('lengthBetween', 6, 15),
                'message' => 'Please input minimum of 6 characters up to 15 characters.'
            )
        )
    );

    public function validatePassword($value, $othervalue) {
        $index = '';
        foreach ($value as $key => $value) {
            $index = $key;
            break;
        }
        debug($value, $othervalue);
        return $this->data[$this->name][$index] === $this->data[$this->name][$othervalue];
    }


    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
}