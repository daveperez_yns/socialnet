<?php
/**
 * Created by PhpStorm.
 * User: YNS
 * Date: 04/05/2017
 * Time: 4:52 PM
 * Author: Dave Perez
 */

App::uses('AppModel', 'Model');

class Post extends AppModel {
    public $validate = array(
        'content' => array(
            'rule' => 'notBlank',
            'message' => 'Please write a content.'
        )
    );
}